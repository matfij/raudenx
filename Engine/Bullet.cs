﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Bullet
    {
        //bullet counter
        private static int bulletCounter = 0;

        //moving direction
        private bool direction;

        //bullet position
        private int bulletX, bulletY;


        public Bullet(bool dir, int bX, int bY)
        {
            this.direction = dir;
            this.bulletX = bX;
            this.bulletY = bY;

            bulletCounter++;
        }


        //updating bullet position
        public void update()
        {
            //move left or right depending who shot
            if(direction)
            {
                bulletX++;
            }
            else
            {
                bulletX--;
            }
        }


        //preparing bullet for being drawn
        public char[,] setPositon(char[,] gameGird)
        {
            char[,] tempGrid = gameGird;

            //setting new player position
            if (direction)
            {
                tempGrid[bulletX, bulletY] = '>';
            }
            else
            {
                tempGrid[bulletX, bulletY] = '<';
            }

            return tempGrid;
        }


        //getters & setters
        public static int getCounter
        {
            get { return bulletCounter; }
            set { bulletCounter = value; }
        }
        public int getX
        {
            get { return bulletX; }
            set { bulletX = value; }
        }
        public int getY
        {
            get { return bulletY; }
            set { bulletY = value; }
        }

    }
}
