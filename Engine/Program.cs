﻿#define DEBUG

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {

            Engine engine1 = new Engine();

            engine1.start();


            #if DEBUG
              Console.WriteLine("\n\nPress enter to close...");
              Console.ReadLine();
            #endif
        }
    }
}
