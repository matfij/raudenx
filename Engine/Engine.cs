﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Engine
    {
        //game window
        private int gameWidth = 200;
        private int gameHeigth = 50;
        private char[,] gameGird = new char[999,999];

        //moving objects
        private Plane playersPlane;
        private Boss bossPlane;
        private List<Bullet> firedBullets = new List<Bullet>();


        //main program thread
        public void start()
        {
            this.setup();

            while(true)
            {
                update();
                update();

                reposition();

                draw();
            }
        }

        //initial values setup & configuration
        public void setup()
        {
            //console parameters
            string consoleTitle = "RaidenX";
            int consoleWidth = gameWidth;
            int consoleHeigth = gameHeigth;

            //players starting position
            int startX = 10;
            int startY = 25;

            //boss starting position
            int bossX = 180;
            int bossY = 25;


            //adjusting properties
            Console.Title = consoleTitle;
            Console.SetWindowSize(consoleWidth, consoleHeigth);

            //initializing players plane
            this.playersPlane = new Plane(startX, startY, gameWidth, gameHeigth);

            //initializing boss plane
            this.bossPlane = new Boss(bossX, bossY, gameWidth, gameHeigth);


        }


        //update method: recalculates new positions
        public void update()
        {
            //moving players plane
            if (Console.KeyAvailable)
            {
                char tempKey = Console.ReadKey().KeyChar;

                playersPlane.move(tempKey);
                
                //shooting
                if(tempKey == 'p')
                {
                    firedBullets.Add(playersPlane.fire());
                }
            }

            //moving boss plane
            bossPlane.move(playersPlane.getX, playersPlane.getY);
            firedBullets.Add(bossPlane.fire());

            //moving bullets
            for (int i = 0; i < Bullet.getCounter; i++)
            {
                if (0 < firedBullets[i].getX && firedBullets[i].getX < 200)
                    firedBullets[i].update();
            }

            //checking for collisons
            playersPlane.collide(firedBullets);
            bossPlane.collide(firedBullets);

        }


        //clearing old positions of moving objects & setting new positions
        public void reposition()
        {
            //clearing old positions
            for (int y = 0; y < 99; y++)
            {
                for (int x = 0; x < 999; x++)
                {
                    gameGird[x, y] = ' ';
                }
            }

            //preparing player for being drawn
            gameGird = playersPlane.setPositon(gameGird);

            //preparing boss for being drawn
            gameGird = bossPlane.setPositon(gameGird);

            //preparing bullets for being drawn
            for (int i = 0; i < Bullet.getCounter; i++)
            {
                if (0 < firedBullets[i].getX && firedBullets[i].getX < 200)
                    gameGird = firedBullets[i].setPositon(gameGird);
            }

        }


        //draw method: redraws console objects
        public void draw()
        {
            //clearing score info
            Console.SetCursorPosition(0, 0);
            Console.Write(new string(' ', Console.WindowWidth));

            //moving console cursor to draw from the same position
            Console.SetCursorPosition(0, 0);
            Console.WriteLine("\t\t HP: " + playersPlane.getHP + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t HP: " + bossPlane.getHP);

            //drawing game field
            for (int y = 0; y < this.gameHeigth; y++)
            {
                for (int x = 0; x < this.gameWidth-10; x++)
                {
                    Console.Write(this.gameGird[x, y]);
                }

                //new row
                Console.Write("\n");
            }

        }
    }
}
