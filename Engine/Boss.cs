﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Boss
    {
        //boss durability
        private int hp = 1000;

        //boss center position
        private int bossX, bossY;

        //game field size
        private int gameX, gameY;


        //constructor with parameters
        public Boss(int bX, int bY, int gX, int gY)
        {
            //setting plane starting position
            this.bossX = bX;
            this.bossY = bY;

            //setting game size
            this.gameX = gX;
            this.gameY = gY;
        }


        //moving method
        public void move(int pX, int pY)
        {
            Random randGenerator = new Random();

            int movePattern = randGenerator.Next(1, 5);

            switch(movePattern)
            {
                case 1:
                    {
                        if (bossY > pY)
                        {
                            int positionFactor = randGenerator.Next(2, 4);

                            bossY -= positionFactor;
                        }
                        else if (bossY < pY)
                        {
                            int positionFactor = randGenerator.Next(2, 4);

                            bossY += positionFactor;
                        }

                        break;
                    }
                case 2:
                case 3:
                    {
                        int positionFactor = randGenerator.Next(1,3);

                        if (positionFactor == 1)
                            bossY++;
                        else
                            bossY--;

                        break;
                    }
                case 4:
                    {
                        int positionFactor = randGenerator.Next(1, 3);

                        if (positionFactor == 1)
                            bossX++;
                        else
                            bossX--;

                        break;
                    }
                default:
                    {
                        break;
                    }
            }

            if (bossY < 3)
                bossY = 5;
            else if (bossY > gameY + 5)
                bossY = gameY - 6;

            if (bossX > gameX - 5)
                bossX = gameX - 5;
            else if (bossX < gameX / 2)
                bossX = gameX / 2;
        }


        //shooting method
        public Bullet fire()
        {
            return new Bullet(false, bossX, bossY);
        }


        //collision detection
        public void collide(List<Bullet> activeBullets)
        {
            for (int i = 0; i < Bullet.getCounter; i++)
            {
                if ((bossX-1 < activeBullets[i].getX && activeBullets[i].getX < bossX+3) &&
                    (bossY-4 < activeBullets[i].getY && activeBullets[i].getY < bossY+4))
                    hp--;
            }
        }


        //prepare for draw
        public char[,] setPositon(char[,] gameGird)
        {
            char[,] tempGrid = gameGird;

            //setting new player position
            tempGrid[bossX, bossY] = '{';
            tempGrid[bossX + 1, bossY] = '=';
            tempGrid[bossX + 2, bossY] = '=';
            tempGrid[bossX + 3, bossY] = '=';
            tempGrid[bossX + 2, bossY - 1] = '[';
            tempGrid[bossX + 2, bossY + 1] = '[';
            tempGrid[bossX + 3, bossY + 1] = '=';
            tempGrid[bossX + 4, bossY - 1] = '=';
            tempGrid[bossX + 4, bossY + 1] = '=';
            tempGrid[bossX + 3, bossY - 1] = '=';
            tempGrid[bossX + 3, bossY + 2] = '{';
            tempGrid[bossX + 3, bossY - 2] = '{';
            tempGrid[bossX + 4, bossY + 2] = '=';
            tempGrid[bossX + 4, bossY - 2] = '=';
            tempGrid[bossX + 5, bossY + 2] = '=';
            tempGrid[bossX + 5, bossY - 2] = '=';

            return tempGrid;
        }


        //getters & setters
        public int getX
        {
            get { return bossX; }
            set { bossX = value; }
        }
        public int getY
        {
            get { return bossY; }
            set { bossY = value; }
        }
        public int getHP
        {
            get { return hp; }
            set { hp = value; }
        }
    }
}
