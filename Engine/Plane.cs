﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    //players plane class: main moving object controlled by player

    class Plane
    {
        //plane durability
        private int hp = 100;

        //game field size
        private int gameX, gameY;

        //plane center position
        private int planeX, planeY;


        //constructor with parameters
        public Plane(int pX, int pY, int gX, int gY)
        {
            //setting plane starting position
            this.planeX = pX;
            this.planeY = pY;

            //setting game size
            this.gameX = gX;
            this.gameY = gY;
        }


        //moving method
        public void move(char key)
        {
            if (key == 'w' && planeY > 1)
                planeY--;
            else if (key == 's' && planeY < gameY - 2)
                planeY++;
            else if (key == 'a' && planeX > 3)
                planeX--;
            else if (key == 'd' && planeX < gameX/2)
                planeX++;
        }


        //shooting method
        public Bullet fire()
        {
            return new Bullet(true, planeX, planeY);
        }


        //collision detection
        public void collide(List<Bullet> activeBullets)
        {
            for (int i = 0; i < Bullet.getCounter; i++)
            {
                if (activeBullets[i].getX == planeX && activeBullets[i].getY == planeY)
                    hp--;
            }
        }


        //prepare for draw
        public char[,] setPositon(char[,] gameGird)
        {
            char[,] tempGrid = gameGird;

            //setting new player position
            tempGrid[planeX, planeY] = '}';
            tempGrid[planeX - 1, planeY] = '=';
            tempGrid[planeX - 2, planeY] = '=';
            tempGrid[planeX - 2, planeY - 1] = ']';
            tempGrid[planeX - 2, planeY + 1] = ']';
            tempGrid[planeX - 3, planeY + 1] = '=';
            tempGrid[planeX - 3, planeY - 1] = '=';

            return tempGrid;
        }


        //getters & setters
        public int getX
        {
            get { return planeX; }
            set { planeX = value; }
        }
        public int getY
        {
            get { return planeY; }
            set { planeY = value; }
        }
        public int getHP
        {
            get { return hp; }
            set { hp = value; }
        }
    }
}
